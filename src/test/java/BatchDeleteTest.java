import com.google.api.client.googleapis.batch.json.JsonBatchCallback;
import com.google.api.client.googleapis.json.GoogleJsonError;
import com.google.api.client.http.HttpHeaders;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import org.junit.Test;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ForkJoinPool;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class BatchDeleteTest {

    private static Logger logger = Logger.getLogger(BatchDeleteTest.class.getName());
    FileHandler fh;
    private static SimpleDateFormat logTime = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
    private static String logFilePath = "googletrash_"+ logTime.format(Calendar.getInstance().getTime()) + ".log";

    static {
        try {
            Files.createFile(Paths.get(logFilePath));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void emptyDriveTrashForkJoin() {

        try {

            String pageToken = null;
            int batchNum = 0;
            int fileListSize = 100;
            String[] gfids = new String[fileListSize];
            int arrayPosition = 0;
            Date date = new Date();


            fh = new FileHandler(logFilePath);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);
            logger.addHandler(fh);

            ForkJoinPool forkJoinPool = new ForkJoinPool();

            GoogleDriveHelper.ConnectToTravisDrive();
            Drive drive = GoogleDriveHelper.getDrive();

            JsonBatchCallback<Void> callback = new JsonBatchCallback<Void>() {
                @Override
                public void onFailure(GoogleJsonError e, HttpHeaders responseHeaders) throws IOException {

                }

                @Override
                public void onSuccess(Void aVoid, HttpHeaders responseHeaders) throws IOException {

                }
            };
            do {
                batchNum++;
                logger.info("Batch num " + batchNum + " started.");

                FileList fileList = drive.files().list()
                        .setQ("trashed=true")
                        .setSpaces("drive")
                        // The page token is needed to iterate through ALL files.
                        .setFields("nextPageToken, files(id)")
                        .setPageToken(pageToken)
                        .setPageSize(fileListSize)
                        .execute();

                pageToken = fileList.getNextPageToken();


                for (File file : fileList.getFiles()) {

                    gfids[arrayPosition] = file.getId();
                    arrayPosition++;
                }
                arrayPosition = 0;

                BatchDelete task = new BatchDelete(gfids, 1, gfids.length, drive, callback, logger, logFilePath);
                forkJoinPool.invoke(task);

                logger.info("Batch num " + batchNum + " completed.");

            } while (!pageToken.equals(null));

        } catch (IOException e) {
            Logger.getLogger(logger.getClass().getName()).log(Level.SEVERE, e + "," + e.getStackTrace());
        }
    }
}
