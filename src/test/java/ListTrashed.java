import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ListTrashed {

    @Test
    public void ListTrashedFiles() throws Exception {

        GoogleDriveHelper.ConnectToTravisDrive();
        Drive drive = GoogleDriveHelper.getDrive();

        String nextPageToken = "";

        FileList fileList = drive.files().list().setFields("files(id, name)").setQ("trashed = true").setPageToken(nextPageToken).execute();
        List<File> files = fileList.getFiles();

        Logger.getLogger(getClass().getName()).log(Level.INFO, "Number of files in Travis Hansen's trash: " + files.size());

        for (File file : files) {
            Logger.getLogger(getClass().getName()).log(Level.INFO, "Name: " + file.getName() + " Google File Id: " + file.getId());
        }

    }
}
