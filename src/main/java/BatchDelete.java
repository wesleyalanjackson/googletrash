import com.google.api.client.googleapis.batch.BatchRequest;
import com.google.api.client.googleapis.batch.json.JsonBatchCallback;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.services.drive.Drive;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.RecursiveAction;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class BatchDelete extends RecursiveAction {

    final int seqThreshold = 5;

    String gfids[];
    int start, end;
    Drive drive = null;
    JsonBatchCallback callback = null;
    FileHandler fh;
    private static String logFilePath = null;
    private static Logger logger = null;


    public BatchDelete(String[] g, int s, int e, Drive d, JsonBatchCallback c, Logger logger, String logFilePath) {
        this.gfids = g;
        this.start = s;
        this.end = e;
        this.drive = d;
        this.callback = c;
        this.logFilePath = logFilePath;
        this.logger = logger;

    }

    protected void compute() {
        if ((end - start) < seqThreshold) {
            BatchRequest batch = drive.batch(new HttpRequestInitializer() {
                @Override
                public void initialize(HttpRequest request) throws IOException {
                    request.setConnectTimeout(10 * 60000);
                    request.setReadTimeout(10 * 60000);
                }
            });
            for (int i = start; i < end; i++) {
                try {
                    drive.files().delete(gfids[i]).queue(batch, callback);
                } catch (IOException e) {
                    logger.severe(e + "," + e.getStackTrace());
                }

            }
            try {
                batch.execute();
                Thread.sleep(2000);
            } catch (IOException | InterruptedException e) {
                logger.severe(e + "," + e.getStackTrace());
            }
        } else {
            int middle = (start + end) / 2;
            invokeAll(new BatchDelete(gfids, start, middle, drive, callback, logger, logFilePath), new BatchDelete(gfids, middle, end, drive, callback, logger, logFilePath));
        }
    }
}